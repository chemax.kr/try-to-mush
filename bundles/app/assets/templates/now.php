<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 17.02.2017
 * Time: 20:40
 */
?>
<?php $this->layout('app:layout'); ?>

<h2><?= $_($message); ?></h2>
<div>
    <div><?= $roomsDescriptions[$character->roomId]->description ?></div>
    <div class="row statusbar">
        <div class="shipstatus col-sm-6">
            <table align="right" border="2">
                <tr>
                    <td>
                        <table align="right">
                            <thead>
                            <tr align="center">
                                <td>armor</td>
                                <td>shield</td>
                                <td>cycle</td>
                                <td>Fuel</td>
                                <td>oxygen</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr align="center">
                                <td><?= $flight->armor ?></td>
                                <td><?= $flight->armor ?></td>
                                <td><?= $flight->cycleCount ?></td>
                                <td><?= $flight->fuel ?></td>
                                <td><?= $flight->oxygen ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="playerStatus col-sm-6">
            <table align="right">
                <thead>
                <tr>
                    <td colspan="5" align="center"><?= $character->characterDescription()->name ?></td>
                </tr>
                <tr align="center">
                    <td>hp</td>
                    <td>morale</td>
                    <td>pa</td>
                    <td>pm</td>
                    <td class="col-md-4">Комната</td>
                </tr>
                </thead>
                <tbody>
                <tr align="center">
                    <td><?= $character->hp ?></td>
                    <td><?= $character->morale ?></td>
                    <td><?= $character->pa ?></td>
                    <td><?= $character->pm ?></td>
                    <td><?= $roomsDescriptions[$character->roomId]->name ?></td>

                </tr>

                </tbody>
            </table>
        </div>
    </div>
    <div class="row room">
        <div class="doors col-sm-2">
            <div align="left">
                <h2>Двери</h2>
                <?php foreach ($doors->asArray(true) as $item) {
                    $visibleDoors[$item->leftRoomId]['id'] = $item->leftRoomId;
                    $visibleDoors[$item->leftRoomId]['status'] = $item->lrStatus;
                    $visibleDoors[$item->leftRoomId]['room'] = $roomsDescriptions[$item->leftRoomId]->name;
                    $visibleDoors[$item->leftRoomId]['url'] = $this->httpPath('app.action', ['processor' => 'flight', 'action' => 'goToDoor', 'id' => $item->id]);
                    $visibleDoors[$item->rightRoomId]['id'] = $item->rightRoomId;
                    $visibleDoors[$item->rightRoomId]['status'] = $item->rlStatus;
                    $visibleDoors[$item->rightRoomId]['room'] = $roomsDescriptions[$item->rightRoomId]->name;
                    $visibleDoors[$item->rightRoomId]['url'] = $this->httpPath('app.action', ['processor' => 'flight', 'action' => 'goToDoor', 'id' => $item->id]);
                }
                unset($visibleDoors[$character->roomId]);

                ?>

                <table border="2">
                    <thead>
                    <tr>

                        <td>Переход</td>
                        <td>Статус</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($visibleDoors as $item) { ?>
                        <tr>


                            <td><a class="nav-link" href="<?= $item['url'] ?>"><?= $item['room'] ?></a></td>
                            <td><?= $item['status'] ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="characters col-sm-5">
            <div>
                <h2>Соседи по комнате</h2>
                <table border="2">
                    <thead>
                    <tr>


                        <td>Имя перса</td>
                        <td>Описание</td>
                        <td>Действия</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($charsInRoom as $char) { ?>
                        <tr>

                            <td><?= $char->characterDescription()->name ?></td>
                            <td><?= $char->characterDescription()->description ?></td>
                            <td>

                                <?php
                            foreach ($charActions[$char->id] as $itemAction) {
                                $url = $this->httpPath('app.action2', ['processor' => 'action', 'action' => 'do', 'act' => $itemAction->name, 'id' => $char->id]);
                                echo "<a href='$url'>" . $itemAction->name . "</a><br>";
                            }

                            ?>

                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="chat col-sm-5">

            <input type="text" id="messageInput"/>
            <input type='submit'
                   onclick='
                           var container = new Array();
                           container[0] = "<?= $character->id ?>";
                           container[1] = "<?= $signature ?>";
                           container[2]= $("#messageInput").val();
                           var json_str = JSON.stringify(container);
                           console.log(json_str);
                           <!--$(conn.send("<?= $character->id . ":" . $signature ?>:"+$("#messageInput").val()));-->
                           $(conn.send(JSON.stringify(container)));
                           $("#messageInput").val("");'/>
            <div class="pre-scrollable col-sm-12">
            <div id="chatPlace">

                <?php foreach ($messages as $message){?>
                    <div class="message"><p>[<?=$message->date?>][<?=$charArr[$message->characterId]->name?>]:</p><p><?=$message->text?></p><p></p></div>
                <?php
                }
                ?>

            </div>
            </div>
        </div>
    </div>
    <div class="row inventory">
        <div class="inventory col-sm-4">
            <table align="right" border="2">
                <tr>
                    <td>Вещь</td>
                    <td>Описание</td>
                    <td>Действия</td>
                </tr>
                <?php foreach ($charItems as $item) { ?>
                    <tr>
                        <td><?= $item->itemDescription()->name ?></td>
                        <td><?= $item->itemDescription()->description ?></td>
                        <td>
                            <?php
                            foreach ($itemActions[$item->id] as $itemAction) {
                                $url = $this->httpPath('app.action2', ['processor' => 'action', 'action' => 'do', 'act' => $itemAction->name, 'id' => $item->id]);
                                echo "<a href='$url'>" . $itemAction->name . "</a><br>";
                            }

                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <div class="roomInventory col-sm-3">
            <table border="2">
                <thead>
                <tr>
                    <td>item name</td>
                    <td>item description</td>
                    <td>Actions</td>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($roomItems as $item) {
                    ?>
                    <tr>
                        <td>
                            <?php
                            echo $item->itemDescription()->name . "</td><td>" . $item->itemDescription()->description;
                            ?>
                        </td>
                        <td>
                            <table>
                                <?php
                                foreach ($itemActions[$item->id] as $itemAction) {
                                    $url = $this->httpPath('app.action2', ['processor' => 'action', 'action' => 'do', 'act' => $itemAction->name, 'id' => $item->id]);
                                    echo "<tr><td><a href='$url'>" . $itemAction->name . "</a></td></tr>";
                                }

                                ?>
                            </table>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="sendMessage col-sm-5">

        </div>
    </div>
</div>


<?php $this->startBlock('scripts'); ?>
<script>
    console.log('<?=$character->id . ':' . $signature?>');

    var conn = new WebSocket('ws://<?=$_SERVER['SERVER_NAME']?>:8080');
    conn.onopen = function (e) {

        console.log("Connection established!");
        console.log(conn);
        conn.send('<?=$character->id . ':' . $signature?>');

    };
    conn.onmessage = function (e) {
        console.log(e.data);

        $('#chatPlace').append(
            '<div class="message">['
            + JSON.parse(e.data)[2]
            + ']['
            + JSON.parse(e.data)[1]
            + ']:'
            + JSON.parse(e.data)[0]
            + '</div>').focus();
//        $('#chatPlace').animate({scrollTop: $('#chatPlace').height()},1000);
    };

</script>

<?php $this->endBlock(); ?>
