<?php

return [
    'relationships' => [

        [
            'type' => 'oneToMany',
            'owner' => 'flight',
            'items'  => 'character'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'character',
            'items'  => 'characterCategory'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'characterDescription',
            'items'  => 'characterDescriptionCategory'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'itemDescription',
            'items'  => 'itemDescriptionCategory'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'item',
            'items'  => 'itemCategory'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'flight',
            'items'  => 'room'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'roomsDescription',
            'items'  => 'room'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'room',
            'items'  => 'door',
            'itemsOptions' => ['ownerKey' => 'leftRoomId', 'ownerProperty' => 'leftRoom'],
            'ownerOptions' => ['itemsProperty' => 'leftDoors'],
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'room',
            'items'  => 'door',
            'itemsOptions' => ['ownerKey' => 'rightRoomId', 'ownerProperty' => 'rightRoom'],
            'ownerOptions' => ['itemsProperty' => 'rightDoors'],

        ],
//        [
//            'type' => 'oneToOne',
//            'owner' => 'character',
//            'item'  => 'characterDescription'
//        ],
        [
            'type' => 'oneToMany',
            'owner' => 'characterDescription',
            'items'  => 'character'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'user',
            'items'  => 'character'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'room',
            'items'  => 'character'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'room',
            'items'  => 'item'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'character',
            'items'  => 'item'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'itemDescription',
            'items'  => 'item'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'action',
            'items'  => 'actionTag'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'action',
            'items'  => 'actionEffect'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'flight',
            'items'  => 'message'
        ],
        [
            'type' => 'oneToMany',
            'owner' => 'character',
            'items'  => 'message'
        ],
    ]
];