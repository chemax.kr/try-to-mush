<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 26.02.2017
 * Time: 16:31
 */
namespace Project\App\Console;

use PHPixie\Console\Command\Config;
use PHPixie\Slice\Data;
//use Project\Framework;
use Ratchet\MessageComponentInterface;
use Ratchet\Server\IoServer;
use Ratchet\ConnectionInterface;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Parsedown;

/**
 * Command to display latest messages in the console
 */
class Chat extends Command
{
    /**
     * Command configuration
     * @param Config $config
     */
    protected function configure($config)
    {
        $config->description("Print latest messages");
    }

    /**
     * @param Data $argumentData
     * @param Data $optionData
     */
    public function run($argumentData, $optionData)
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new ChatSocket($this->builder)
                )
            ),
            8080
        );

        $server->run();
    }
}

class ChatSocket implements MessageComponentInterface
{
    protected $clients;
    protected $builder;
    protected $characters;
    protected $flight;

    public function __construct($builder)
    {
        $this->clients = new \SplObjectStorage;
        $this->builder = $builder;
        $this->characters = "";
        $this->flight = "";
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        var_dump($msg);
//        $token = explode(":", $msg);
        $token = json_decode($msg);
        print_r($token);
        if ((md5($token[0] . 'abrakadabra') == $token[1]) && ($token[2] != "")) {
            $orm = $this->builder->components()->orm();
            $Parsedown = new Parsedown();
            $message[0] = $Parsedown->text(htmlspecialchars(htmlentities(strip_tags($token[2]), ENT_QUOTES, "UTF-8"), ENT_QUOTES));


            if (!isset($from->flightId)) {

                $character = $orm->query('character')->where('id', $token[0])->findOne();
                $flightId = $character->flightId;
                $from->characterId = $token[0];
                $from->characterDescription = $character->characterDescription();
                echo $from->characterDescription->name;
            } else {
                $flightId = $from->flightId;
            }
            if (!isset($this->flight[$flightId])) {
                $this->flight[$flightId] = new \SplObjectStorage;
            }


            if (!$this->flight[$flightId]->contains($from)) {
                echo "not in array";
                $from->flightId = $flightId;
                $this->flight[$flightId]->attach($from);
            } else {
                echo sprintf('characterID %d шлет сообщение для flightId %d: %s' . "\n"
                    , $from->characterId, $from->flightId, $msg == 1 ? '' : 's');
            }
//            $flight[$character->flightId][]=$from;
            echo $from->characterId . "\n";
//            echo  $character->characterDescription()->name."\n";
            $numRecv = count($this->clients) - 1;
            echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
                , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');
            $d = date("Y-m-d H:i:s");
            $message[1] = $from->characterDescription->name;
            $message[2] = $d;
            foreach ($this->flight[$flightId] as $client) {

                // The sender is not the receiver, send to each client connected
                $client->send(json_encode($message));

            }
            echo json_encode($message);
            $message = $orm->createEntity('message', [
                'characterId'  =>  $from->characterId,
                'flightId'  =>  $flightId,
                'text'  =>  $message[0],

                ]);
            $message->save();
            $message = NULL;


        } else {
        }

    }

    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}