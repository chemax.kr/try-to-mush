<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 24.02.2017
 * Time: 11:14
 */
namespace Project\App\HTTP;


class Action extends Processor
#\PHPixie\DefaultBundle\Processor\HTTP\Actions
{
    protected $builder;
    protected $orm;
    protected $components;

//    protected $config = new \PHPixie\Config();
    public function __construct($builder)
    {
        $this->builder = $builder;
        $this->orm = $this->builder->components()->orm();
        $this->components = $this->builder->components();
    }

    public function components()
    {
        return $this->builder->components();
    }

    protected function authDomain()
    {
        return $this->builder->components()->auth()->domain();
    }


    public function defaultAction()
    {
        return "d";
    }

    public function doAction($request)
    {
        $orm = $this->builder->components()->orm();
        $user = $this->checkAccessUser($request);
        $character = $orm->query('character')->where('userId', $user->id)->findOne();
        $room = $character->room();
        $id = $request->attributes()->get('id');
        $act = $request->attributes()->get('act');
        $item = $orm->query('item')->in($id)->findOne();
        $url = "/flight/now";
        $database = $this->builder->components()->database();
        $database->get()->beginTransaction();
        try {
            switch ($act) {
                case "pickup":
                    $item->characterId = $character->id;
                    $item->roomId = NULL;
                    $item->save();
//                    $this->addCategoryTo($character, $item);
                    break;
                case "drop":
                    $item->roomId = $room->id;
                    $item->characterId = NULL;
//                    $this->removeCategoryFrom($character, $item);
                    $item->save();
                    break;
                default:
//                    echo "pickup";
                    $action = $orm->query('action')->where('name', $act)->findOne();
                    $effects = $action->actionEffects();
                    foreach ($effects as $effect) {
                        $this->takeEffect($effect, $id, $request);
                    }

                    break;
            }

            $database->get()->commitTransaction();

        } catch (\Exception $e) {
            $database->get()->rollbackTransaction();
            echo "error";
            throw $e;
        }
        $this->redirectTo($url, $request);
//        return $id.$act;
    }

    protected function addCategoryTo($character, $item)
    {
        $cat = $this->components()->orm()->createEntity('characterCategories', [
            'name' => $item->itemDescription()->name,
            'characterId' => $character->id
        ]);
        $cat->save();
//        $itm = $item->itemDescription()->name;
//        $jsonTMP[] = $itm;

//        $character->category = json_encode($jsonTMP);
//        $character->save();
    }

    protected function removeCategoryFrom($character, $item)
    {
        $cat = $this
            ->orm
            ->query('characterCategory')
            ->where('characterId', $character->id)
            ->and('name', $item->name)
            ->findOne()
            ->delete();
//        print_r($cat);
    }

    protected function takeEffect($effect, $id, $request)
    {
        $orm = $this->builder->components()->orm();
        $user = $this->checkAccessUser($request);
        $effectTarget = $effect->effectTarget;
//        echo "<pre>";
//        print_r($effect->asObject());
//
//        echo "</pre>";
        switch ($effect->effectType) {

            case "selfProperty":
                $character = $orm->query('character')->where('userId', $user->id)->findOne();
                $fieldVal = $character->getField($effectTarget);
                $character->setField($effectTarget, ($fieldVal + $effect->effectValue));
                $character->save();
                break;
            case "targetProperty":
                $character = $orm->query('character')->where('id', $id)->findOne();
                $fieldVal = $character->getField($effectTarget);
                $character->setField($effectTarget, ($fieldVal + $effect->effectValue));
                $character->save();
                break;
            case "createItem":
                $character = $orm->query('character')->where('userId', $user->id)->findOne();
                $room = $character->room();
                $itemDescr = $orm->query('itemDescription')->in($effect->effectValue)->findOne();
                $item = $this->components()->orm()->createEntity('item', [
                    'roomId' => $room->id,
                    'itemDescriptionId' => $itemDescr->id,
                    'itemCharge' => $itemDescr->chargesStart,
                    'itemChargeMax' => $itemDescr->chargesMax
                ]);
                $item->save();
                break;
            case "removeItem":
                if ($effect->effectValue) {
                    $character = $orm->query('character')->where('userId', $user->id)->findOne();
                    $room = $character->room();
                    $orm->query('item')->where('itemDescriptionId', $effect->effectValue)->and('roomId', $room->id)->or('characterId', $character->id)->findOne()->delete();
                } else {
                    $orm->query('item')->in($id)->findOne()->delete();
                }
                break;
        }
    }

    public function testAction($request)
    {
        $id = $request->attributes()->get('id');
        $act = $request->attributes()->get('act');
        $actions = $this->getListFor($id, "character");
        foreach ($actions as $act) {
            echo $act->name;
            echo "<br />";
        }

    }

    public function getListFor($id, $type = "item")
    {
        /*
         * Чтобы получить список возможных действий для объекта нам надо про него знать:
         * в какой комнате он находится (комната добавляется как категория во временный массив)
         * Список доступных ему категорий из базы
         * Список категорий персонажа который пытается взаимодействовать с объектом
         * Всё это собирается в один массив категорий.
         * Далее, мы получаем список действий в условиях которых есть хотя бы одна категория из этого массива.
         * После этого, мы проверяем каждое действие на безостаточное вхождение в массив категорий.
         * Те действия, чьи категории (все) есть в массиве - условно доступны для объекта.
         *
         */
//        $actions = "";
        $user = $this->user();
        $character = $this->orm->query('character')->in($user->characterId)->findOne();
        $actionId = "";
//        echo $id;
        if (!$id) {
            return "";
        }
        $item = $this->orm->query($type)->in($id)->findOne();
        if ($type == "character") {
            $getCategories = $item->characterDescription()->characterDescriptionCategories();

            $inventory = $character->items();
            foreach ($inventory as $oneitem) {

                $categories[] = $oneitem->itemDescription()->name;
            }
        } elseif ($type == "item") {
            $getCategories = $item->itemDescription()->itemDescriptionCategories();
        }
        foreach ($getCategories as $category) //из орм нам приходит объект, нам же нужен обычный одномерный массив
        {
            $categories[] = $category->name;
        }

        //Если это вещь то она может лежать в комнате или в кармане(инвенторе) игрока
        if ($item->roomId) //Если она в комнате то тут будет ID, иначе null
        {
            $categories[] = "room"; //раз лежит в комнате, значит надо это отразить
            $categories[] = $this//у комнаты есть роль, от этой роли набор действий тоже зависит, добавляем в категории
            ->orm
                ->query('room')
                ->in($item->roomId
                )->findOne()
                ->roomsDescription()
                ->roleType;
        } elseif ($item->characterId) //Если roomId отсутствует, вещь может лежать в кармане персонажа.
        {
            $categories[] = "inventory"; // Что мы и указываем
        } else {

        }
        $actionTags = $this//получаем список действий в условиях которых есть хотя бы одна категория из этого массива.
        ->orm
            ->query('actionTag')
            ->where('tag', 'in', $categories)
            ->find();
        foreach ($actionTags as $tag)   //Пересобираем в одномерный массив
            //получая ID тех Action к которым принадлежат эти таги
        {
            $actionId[] = $tag->actionId;
        }
        //Нужно проверить каких элементов из массива тагов нет в категориях предмета.
        // Если не нулл, значит действие недоступно.
        $actions = $this
            ->orm
            ->query('action')
            ->in($actionId)
            ->find();

        foreach ($actions as $action) {
            $tags = $action->actionTags();
            foreach ($tags as $tag) {
                $tagArr[$action->id][] = $tag->tag;
            }
//            echo "<pre>";
//            echo $action->name . "<br>";
//            print_r(count(array_diff($tagArr[$action->id], $categories)));
//            echo "<br>";
//            print_r(array_diff($tagArr[$action->id], $categories));

            if (count(array_diff($tagArr[$action->id], $categories)) == 0) {
//                echo "<p>added " . $action->name . " </p>";
                $actionsArr[] = $action;
            }
        }
//echo "==========";
//        print_r($categories);
//        echo "==========";
//        print_r($tagArr);

        return $actionsArr;

    }


}