<?php

namespace Project\App\HTTP;

use Project\App\AppBuilder;
use Project\App\ORM\User;

/**
 * Your base web processor class
 */
abstract class Processor extends \PHPixie\DefaultBundle\HTTP\Processor
{
    /**
     * @var AppBuilder
     */
    protected $builder;
    protected $orm;
    protected $components;

    /**
     * @param AppBuilder $builder
     */
    public function __construct($builder)
    {
        $this->builder = $builder;
        $this->orm = $this->builder->components()->orm();
        $this->components = $this->builder->components();
    }

    /**
     * Return a user if he is logged in, or null otherwise
     *
     * @return User|null
     */
    protected function user()
    {
        $domain = $this->components()->auth()->domain();
        return $domain->user();
    }

    protected function checkAccessUser($request)
    {

        $user = $this->user();
        if($user){
            return $user;

        }
        else{
            $http = $this->builder->components()->http();
            $responses = $http->responses();
            $url = '/auth';
            $response = $responses->redirect($url);
            $context = $http->context($request);
            $http->output(
                $response,
                $context
            );
            exit;
        }
    }
    protected function redirectTo($url, $request){
        $http = $this->builder->components()->http();
        $responses = $http->responses();
        //$url = '/auth';
        $response = $responses->redirect($url);
        $context = $http->context($request);
        $http->output(
            $response,
            $context
        );
        exit;
    }
}