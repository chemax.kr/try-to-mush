<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 08.01.2017
 * Time: 22:10
 */
#namespace Project\App\HTTPProcessors;
namespace Project\App\HTTP;


use PHPixie\Validate\Errors\Error\Message;

class Flight extends Processor
    #\PHPixie\DefaultBundle\Processor\HTTP\Actions
{
    protected $builder;
    protected $orm;
    protected $components;

//    protected $config = new \PHPixie\Config();
    public function __construct($builder)
    {
        $this->builder = $builder;
        $this->orm = $this->builder->components()->orm();
        $this->components = $this->builder->components();

    }

    public function components()
    {
        return $this->builder->components();
    }

    protected function authDomain()
    {
        return $this->builder->components()->auth()->domain();
    }


    public function defaultAction($request)
    {
        $user = $this->checkAccessUser($request);
        if ($user->characterId != NULL) {
            {
                $http = $this->builder->components()->http();
                $responses = $http->responses();
                $url = '/flight/now';
                $response = $responses->redirect($url);
                $context = $http->context($request);
                $http->output(
                    $response,
                    $context
                );
                exit;
            }
        }

//        $id=1;
        $orm = $this->builder->components()->orm();
//        $shipRepo = $orm->repository('flight');
        $characters = $orm->query('character')
            ->where('userId', '0')
            ->orderBy('dateStart', 'desc')
            ->find();

        if (count($characters->asArray(true, 'id')) > 0) {
            $container = $this->components()->template()->get('app:greet');
            $container->message = 'Выберите персонажа';
            $container->characters = $characters;
            $container->user = $user;
            return $container;
        } else $this->createNewAction($request);
    }

    public function redirectAction($request)
    {

        $this->redirectTo('/flight', $request);

    }

    public function selectAction($request)
    {
        $user = $this->checkAccessUser($request);
        if ($user->characterId != NULL) {
            $this->redirectTo('/flight/now', $request);
        }
        $orm = $this->builder->components()->orm();
        $userId = $user->id;
        $id = $request->attributes()->get('id');
        $charEntity = $orm->query('character')->in($id)->findOne();
        $charEntity->userId = $userId;
        $charEntity->save();
        $user->characterId = $charEntity->id;
        $user->save();
        $this->redirectTo('/flight/now', $request);
    }

    public function newAction($request)
    {
        return "Тут будет выбор опций для нового корабля";
    }

    public function nowAction($request)
    {
        $user = $this->checkAccessUser($request);
        $components = $this->components();
        $orm = $this->builder->components()->orm();
        $character = $orm->query('character')->where('userId', $user->id)->findOne();
        $signature = md5($character->id . 'abrakadabra');
        $characterDescription = $character->characterDescription();
        $flight = $character->flight();
        $characters = $flight->characters();
        $messages = $flight->messages();
        $messageQuery = $components->orm()->query('message')
            ->where('flightId', $flight->id)
            ->orderDescendingBy('date');

        $pager = $components->paginateOrm()
            ->queryPager($messageQuery, 10, ['user']);

        $page = $request->attributes()->get('page', 1);
        $pager->setCurrentPage($page);

        $container = $this->components()->template()->get('app:now', [
            'pager' => $pager,
            'user' => $this->user()
        ]);
        $container->messages = $messages;
        $container->room = $character->room();
        $container->doors = $orm->query('door')->where('leftRoomId', $character->room()->id)->or('rightRoomId', $character->room()->id)->find();
        $roomItems = $character->room()->items();
        $container->roomItems = $roomItems;
        $itemActions = NULL;
        $characterActions = NULL;
        foreach ($roomItems as $item) {
            $itemActions[$item->id] = $this->builder->httpProcessor()->processor('action')->getListFor($item->id);
            //$builder->httpProcessor()->processor('some')->bla();
        }
        $charItems = $character->items();
        $container->charItems = $charItems;
        foreach ($characters as $c) {
            $charArr[$c->id] = $c->characterDescription();
        }
        $container->charArr = $charArr;
        foreach ($charItems as $item) {
            $itemActions[$item->id] = $this->builder->httpProcessor()->processor('action')->getListFor($item->id);
        }
        $container->itemActions = $itemActions;
        $container->message = "текущий полет игрока";
//        $container->rooms = $rooms;
        $container->flight = $flight;
        $container->characterDescription = $characterDescription;
        $container->character = $character;
        $container->characters = $flight->characters();
        $container->user = $user;
        $container->signature = $signature;
        $charsInRoom = $orm->query('character')->where('roomId', $character->room()->id)->find();
        $container->charsInRoom = $charsInRoom;
        foreach ($charsInRoom as $char)
        {
            $charActions[$char->id] = $this->builder->httpProcessor()->processor('action')->getListFor($char->id, "character");
        }
        $container->charActions = $charActions;
        $container->roomsDescriptions = $orm->query('roomsDescription')->find()->asArray(true, 'id');

        return $container;
    }

    public function testAction($request)
    {
        $id = $request->attributes()->get('id');
        return $this->builder->httpProcessor()->processor('action')->getListFor($id, "character");
//        return $this->builder->httpProcessor()->processor('action')->getListFor($id, "character");
//        return $this->getListFor($id, "character");
    }


    public function goToDoorAction($request)
    {
        $id = $request->attributes()->get('id');
        $user = $this->checkAccessUser($request);
        $orm = $this->builder->components()->orm();
        $char = $orm->query('character')->where('userId', $user->id)->findOne();
        //var_dump($char->id);
        $room = $char->room();
        $door = $orm->query('door')->where('id', $id)->findOne()->asObject();

        if (($room->id == $door->leftRoomId)) {
            $char->roomId = $door->rightRoomId;
            $char->pm--;
            $char->save();
        } elseif (($room->id == $door->rightRoomId)) {
            $char->roomId = $door->leftRoomId;
            $char->pm--;
            $char->save();
        }
        $this->redirectTo('/flight/now', $request);
    }

    public function createNewAction($request)
    {
        $database = $this->builder->components()->database();
        $database->get()->beginTransaction();
        try {
            $shipTypeId = 0;
            $userId = 0;
            if ($request->attributes()->get('id')) {
                $shipTypeId = $request->attributes()->get('id');
            }
            $flight = $this->components()->orm()->createEntity('flight', [
                'statusId' => 0,
                'shipTypeId' => $shipTypeId,
                'missionId' => 0
            ]);
            $flight->save();
//        $room = $flight->rooms()
            $crew = $this->components()->orm()->query('characterDescription')->find();

            $roomsDescription = $this->components()->orm()->query('roomsDescription')->find()->asArray(true, 'id');
            $itemDescriptions = $this->components()->orm()->query('itemDescription')->find();

            for ($i = 0; $i < 4; $i++) {
                $room = $this->components()->orm()->createEntity('room', [
                    'roomsDescriptionId' => $i + 1,
                    'flightId' => $flight->id,
                    'name' => $roomsDescription[$i + 1]->name
                ]);
                $room->save();

                if (($room->roomsDescriptionId) == 1) {
                    foreach ($crew->asArray(true) as $crewMember) {
                        $rand = rand(1, 4);
                        if ($rand == 1) {
                            $isMush = "[\"mush\"]";
                        } else {
                            $isMush = NULL;
                        }
                        $member = $this->components()->orm()->createEntity('character', [
                            'flightId' => $flight->id,
                            'userId' => $userId,
                            'characterDescriptionId' => $crewMember->id,
//                        'category' => $isMush,
                            'roomId' => $room->id
                        ]);
                        $member->save();
                        $cats = $member->characterDescription()->characterDescriptionCategories();
                        foreach ($cats as $cat) {
                            $this->orm->createEntity('characterCategory', [
                                'characterId' => $member->id,
                                'name' => $cat->name
                            ])->save();

                        }
                    }

                }
            }

            $rooms = $this->components()->orm()->query('room')->where('flightId', $flight->id)->find();
            $roomsArray = $rooms->asArray(true, 'id');
            foreach ($rooms->asArray(true) as $room) {
                do {

                    $right = array_rand($roomsArray);

                } while ($right == $room->id);
                $door = $this->components()->orm()->createEntity('door', [
                    'leftRoomId' => $room->id,
                    'rightRoomId' => $right,
                    'lrStatus' => '',
                    'rlStatus' => ''
                ]);
                $door->save();
                $doors[] = $door->asObject(true);

                foreach ($itemDescriptions as $itemDescr) {
                    $rand = rand(1, 2);
                    if ($rand == 1) {
                        $item = $this->components()->orm()->createEntity('item', [
                            'roomId' => $room->id,
                            'itemDescriptionId' => $itemDescr->id,
                            'itemCharge' => $itemDescr->chargesStart,
                            'itemChargeMax' => $itemDescr->chargesMax
                        ]);
                        $item->save();
                        $cats = $item->itemDescription()->itemDescriptionCategories();
                        foreach ($cats as $cat) {
                            $this->orm->createEntity('itemCategory', [
                                'itemId' => $member->id,
                                'name' => $cat->name
                            ])->save();

                        }
                    }

                }

            }
//        print_r($doors);
            $database->get()->commitTransaction();
        } catch (\Exception $e) {
            $database->get()->rollbackTransaction();
            echo "error ";
            throw $e;
        }
        return "ok";
        $this->redirectTo('/flight', $request);

    }

    protected function setSelectedShip($user, $choose)
    {
        print_r($user);
    }
}