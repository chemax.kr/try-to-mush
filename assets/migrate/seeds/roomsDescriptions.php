<?php
/**
 * User: zork
 * Date: 18.02.2017
 * Time: 11:23
 */
return [
    [
        'name' => 'Лаборатория',
        'description' => 'Склянки, приборы, светодиоды и всё такое понятное только местному персоналу. Остальные стараются особенно не торчать в этой комнате, боясь что-то не там нажать или сломать ненароком. Всё-таки цена оборудования здесь сопоставима с ценой корабля в целом. Главное, найти ей нужное применение.',
        'roleType' => 'laboratory'
    ],
    [
        'name' => 'Коридор',
        'description' => 'Обычно чистый и хорошо освещенный коридор с гладкими поверхностями. Фальш стены хорошо спрятаны от постороннего взгляда. Гармонию частично нарушают только цветовые полосы обозначающиее переход в тот или иной отсек корабля.',
        'roleType' => 'corridor'
    ],
    [
        'name' => 'Мостик',
        'description' => 'Большие экраны вместо иллюминаторов закрывают собой большую часть пространства на стенах. Несколько терминалов и голограмма завершают образ среднестатистического мостика. Но главное, самое главное, это удобное капитанское кресло в центре на небольшом постаменте. Всё, что нужно для управления кораблём, собранов одной комнате.',
        'roleType' => 'bridge'
    ],
    [
        'name' => 'Хранилище',
        'description' => 'Несколько рядов стелажей, всякие запчасти и коробки с консервами. Это всё, что нужно знать об этой комнате. Дополнением оказывается вечно скапливающаяся здесь пыль. Не смотря на наличие вполне рабочих воздушных фильтров. Так же, все знаю, что где-то здесь должна быть спрятана заначка спиртного. Но вот уже который год не могут найти.',
        'roleType' => 'storage'
    ],
];