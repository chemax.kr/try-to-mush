<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 25.02.2017
 * Time: 16:05
 *   `actionId` int(11) NOT NULL,
`effectType` varchar(50) NOT NULL,
`effectTarget` varchar(50) DEFAULT NULL,
`effectValue` varchar(50) NOT NULL
 */
return [
    [
        'actionId'     => 3,
        'effectType'   => "removeItem",
    ],
    [
        'actionId'     => 4,
        'effectType'   => "removeItem",
    ],
    [
        'actionId'     => 5,
        'effectType'   => "removeItem",
    ],
    [
        'actionId'     => 5,
        'effectType'   => "selfProperty",
        'effectTarget'   => "pa",
        'effectValue'   => 2,
    ],
    [
        'actionId'     => 7,
        'effectType'   => "targetProperty",
        'effectTarget'   => "hp",
        'effectValue'   => 3,
    ],
    [
        'actionId'     => 6,
        'effectType'   => "targetProperty",
        'effectTarget'   => "hp",
        'effectValue'   => -3,
    ],
];