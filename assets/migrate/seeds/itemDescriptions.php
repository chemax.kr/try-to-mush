<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 24.02.2017
 * Time: 11:40
 */

return [
    [
        'id'    =>  1,
        'name'     => 'coffee',
        'description' => 'cap of coffee',
//        'chargesStart'   => "Hello World!",
//        'chargesMax'   => '2016-12-01 10:15:00',
        'typeId'  =>  '1', //1 food
        'category'  =>  json_encode(["item", "food"]),
//        'canStack'  =>  ''
    ],
    [
        'id'    =>  2,
        'name'     => 'rations',
        'description' => 'taste and consistency of the old sole',
//        'chargesStart'   => "Hello World!",
//        'chargesMax'   => '2016-12-01 10:15:00',
        'typeId'  =>  '1', //1 food
        'category'  =>  json_encode(["item", "food"]),
//        'canStack'  =>  ''
    ],
    [
        'id'    =>  3,
        'name'     => 'communicator',
        'description' => 'your personal e-assistent',
//        'chargesStart'   => "Hello World!",
//        'chargesMax'   => '2016-12-01 10:15:00',
        'typeId'  =>  '2', //2 electronics
        'category'  =>  json_encode(["item", "electronics"]),
//        'canStack'  =>  ''
    ],
    [
        'id'    =>  4,
        'name'     => 'medikit',
        'description' => 'first aid complex',
//        'chargesStart'   => "Hello World!",
//        'chargesMax'   => '2016-12-01 10:15:00',
        'typeId'  =>  '3', //3 medical
        'category'  =>  json_encode(["item", "medikit"]),
//        'canStack'  =>  ''
    ],
    [
        'id'    =>  5,
        'name'     => 'Adjustable Wrench',
        'description' => 'Adjustable Wrench',
//        'chargesStart'   => "Hello World!",
//        'chargesMax'   => '2016-12-01 10:15:00',
        'typeId'  =>  '4', //4 instrument
        'category'  =>  json_encode(["item", "instrument", "mechanical"]),
//        'canStack'  =>  ''
    ],
    ];