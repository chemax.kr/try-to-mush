<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 24.02.2017
 * Time: 18:33
 */
return [
    [
        'id'     => 1,
        'name'   => "pickup",
    ],
    [
        'id'     => 2,
        'name'   => "drop",
    ],
    [
        'id'     => 3,
        'name'   => "hide",
    ],
    [
        'id'     => 4,
        'name'   => "dismantle",
    ],
    [
        'id'     => 5,
        'name'   => "eat",
    ],
    [
        'id'     => 6,
        'name'   => "hit",
    ],
    [
        'id'     => 7,
        'name'   => "heal",
    ],
];