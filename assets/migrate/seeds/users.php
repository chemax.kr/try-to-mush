<?php

return [
    [
        'name' => 'zork',
        'email' => 'zork@chemax24.ru',

        // Password is '1', the hash can be generated like this:
        // password_hash('1', PASSWORD_DEFAULT);
        'passwordHash' => '$2y$10$rYHbaIdHJPGbaG8cgSsV9uplT5Mjr2aFAAGxFX50auip6W/Qv58Y.'
    ],

    [
        'name' => 'bakkuz',
        'email' => 'bakkuz@chemax24.ru',
        'passwordHash' => '$2y$10$rYHbaIdHJPGbaG8cgSsV9uplT5Mjr2aFAAGxFX50auip6W/Qv58Y.'
    ],
    [
        'name' => 'chemax',
        'passwordHash' => '$2y$10$RcWfz03WH8bPd2WHrENt8.AAZLCT3gvGergSmelTjDlitg/UsUGwG',
        'email' => 'chemax@chemax24.ru'
    ],
    [
        'name' => 'kon',
        'passwordHash' => '$2y$10$RcWfz03WH8bPd2WHrENt8.AAZLCT3gvGergSmelTjDlitg/UsUGwG',
        'email' => 'kon@chemax24.ru'
    ],
    [
        'name' => 'xelnod',
        'passwordHash' => '$2y$10$RcWfz03WH8bPd2WHrENt8.AAZLCT3gvGergSmelTjDlitg/UsUGwG',
        'email' => 'xelnod@chemax24.ru'
    ],
    [
        'name' => 'tamtamchik',
        'passwordHash' => '$2y$10$RcWfz03WH8bPd2WHrENt8.AAZLCT3gvGergSmelTjDlitg/UsUGwG',
        'email' => 'tamtamchik@chemax24.ru'
    ],
    [
        'name' => 'aristarskaya',
        'passwordHash' => '$2y$10$RcWfz03WH8bPd2WHrENt8.AAZLCT3gvGergSmelTjDlitg/UsUGwG',
        'email' => 'aristarskaya@chemax24.ru'
    ],
    [
        'name' => 'Tikhiy',
        'passwordHash' => '$2y$10$RcWfz03WH8bPd2WHrENt8.AAZLCT3gvGergSmelTjDlitg/UsUGwG',
        'email' => 'Tikhiy@chemax24.ru'
    ],
    [
        'name' => 'insoln',
        'passwordHash' => '$2y$10$RcWfz03WH8bPd2WHrENt8.AAZLCT3gvGergSmelTjDlitg/UsUGwG',
        'email' => 'insoln@chemax24.ru'
    ],
    [
        'name' => 'i-zu',
        'passwordHash' => '$2y$10$RcWfz03WH8bPd2WHrENt8.AAZLCT3gvGergSmelTjDlitg/UsUGwG',
        'email' => 'i-zu@protonmail.ch'
    ],
];