<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 12.03.2017
 * Time: 00:23
 */

return [
    [
        'itemDescriptionId' => 1,
        'name' => 'food',
    ],
    [
        'itemDescriptionId' => 1,
        'name' => 'item',
    ],
    [
        'itemDescriptionId' => 2,
        'name' => 'item',
    ],
    [
        'itemDescriptionId' => 2,
        'name' => 'food',
    ],
    [
        'itemDescriptionId' => 3,
        'name' => 'electronics',
    ],
    [
        'itemDescriptionId' => 3,
        'name' => 'item',
    ],
    [
        'itemDescriptionId' => 4,
        'name' => 'item',
    ],
    [
        'itemDescriptionId' => 4,
        'name' => 'medikit',
    ],
    [
        'itemDescriptionId' => 5,
        'name' => 'instrument',
    ],

    [
        'itemDescriptionId' => 5,
        'name' => 'item',
    ],
    [
        'itemDescriptionId' => 5,
        'name' => 'mechanical',
    ],
];
