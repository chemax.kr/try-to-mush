<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 24.02.2017
 * Time: 19:09
 */

return [
    [
        'actionId'  =>  1,
        'tag'   => "item",
    ],
    [
        'actionId'  =>  1,
        'tag'   => "room",
    ],
    [
        'actionId'  =>  2,
        'tag'   => "item",
    ],
    [
        'actionId'  =>  2,
        'tag'   => "inventory",
    ],
    [
        'actionId'  =>  3,
        'tag'   => "item",
    ],
    [
        'actionId'  =>  4,
        'tag'   => "item",
    ],
    [
        'actionId'  =>  4,
        'tag'   => "mechanical",
    ],
    [
        'actionId'  =>  5,
        'tag'   => "food",
    ],
    [
        'actionId'  =>  6,
        'tag'   => "human",
    ],
    [
        'actionId'  =>  7,
        'tag'   => "human",
    ],
    [
        'actionId'  =>  7,
        'tag'   => "medikit",
    ],
];