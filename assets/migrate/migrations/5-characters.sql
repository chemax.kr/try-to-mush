CREATE TABLE `characterDescriptions` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `characterDescriptions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `characterDescriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `characters` (
  `id` int(11) NOT NULL,
  `flightId` int(11) NOT NULL DEFAULT '0',
  `userId` int(11) NOT NULL DEFAULT '0',
  `characterDescriptionId` int(11) NOT NULL DEFAULT '0',
  `hp` int(11) NOT NULL DEFAULT '10',
  `morale` int(11) NOT NULL DEFAULT '10',
  `pa` int(11) NOT NULL DEFAULT '10',
  `pm` int(11) NOT NULL DEFAULT '10',
  `roomId` int(11) NOT NULL DEFAULT '0',

  FOREIGN KEY (characterDescriptionId)
      REFERENCES characterDescriptions(id),

  FOREIGN KEY (userId)
      REFERENCES users(id),

  FOREIGN KEY (flightId)
      REFERENCES flights(id),

  FOREIGN KEY (roomId)
      REFERENCES rooms(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `characters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE characters ADD COLUMN status  VARCHAR(50) NULL AFTER id;
ALTER TABLE characters ADD COLUMN dateStart  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER id;

-- statement

CREATE TABLE messages(
  id INT PRIMARY KEY AUTO_INCREMENT,
  characterId INT NOT NULL,
  flightId INT NOT NULL DEFAULT 0,
  text VARCHAR(255) NOT NULL,
  date DATETIME NOT NULL,

  FOREIGN KEY (characterId)
      REFERENCES characters(id)
);

ALTER TABLE `messages` CHANGE `date` `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP();