CREATE TABLE `actionEffects` (
  `id` int(11) NOT NULL,
  `actionId` int(11) NOT NULL,
  `effectType` varchar(50) DEFAULT NULL,
  `effectTarget` varchar(50) DEFAULT NULL,
  `effectValue` varchar(50) DEFAULT NULL,
FOREIGN KEY (actionId)
  REFERENCES actions(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `actionEffects`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `actionEffects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;