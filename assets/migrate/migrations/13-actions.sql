CREATE TABLE `actions` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- statement

CREATE TABLE `actionTags` (
  `id` int(11) NOT NULL,
  `actionId` int(11) NOT NULL,
  `tag` varchar(40) NOT NULL,
  FOREIGN KEY (actionId)
  REFERENCES actions(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `actionTags`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `actionTags` ADD FULLTEXT KEY `tag` (`tag`);

ALTER TABLE `actionTags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

