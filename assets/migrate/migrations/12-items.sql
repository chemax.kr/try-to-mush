CREATE TABLE `itemDescriptions` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `chargesStart` int(11) DEFAULT NULL,
  `chargesMax` int(11) DEFAULT NULL,
  `typeId` int(11) NOT NULL DEFAULT '0',
  `canStack` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `itemDescriptions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `itemDescriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- statement

ALTER TABLE `itemDescriptions` ADD `category` TEXT NULL AFTER `name`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `characterId` int(11) DEFAULT NULL,
  `roomId` int(11) DEFAULT NULL,
  `itemDescriptionId` int(11) DEFAULT NULL,
  `itemStatus` varchar(10) DEFAULT NULL,
  `itemCharge` int(11) DEFAULT NULL,
  `itemChargeMax` int(11) DEFAULT NULL,
  FOREIGN KEY (roomId)
  REFERENCES rooms(id),
  FOREIGN KEY (itemDescriptionId)
  REFERENCES itemDescriptions(id),
  FOREIGN KEY (characterId)
  REFERENCES characters(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
