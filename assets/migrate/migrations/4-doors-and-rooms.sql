CREATE TABLE `roomsDescriptions` (
`id` INT NOT NULL AUTO_INCREMENT ,
`roleType` VARCHAR(10) NULL ,
`description` TEXT NULL ,
`name` VARCHAR(30) NOT NULL ,
`sizeX` INT NOT NULL DEFAULT '4' ,
`sizeY` INT NOT NULL DEFAULT '4' ,
PRIMARY KEY (`id`)
) ENGINE = InnoDB;


CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `roomsDescriptionId` int(11) NOT NULL,
  `flightId` int(11) NOT NULL,
  FOREIGN KEY (flightId)
      REFERENCES flights(id),
      FOREIGN KEY (roomsDescriptionId)
      REFERENCES roomsDescriptions(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- statement

CREATE TABLE `doors` (
  `id` int(11) NOT NULL,
  `leftRoomId` int(11) NOT NULL,
  `rightRoomId` int(11) NOT NULL,
  `lrStatus` text,
  `rlStatus` text,

  FOREIGN KEY (leftRoomId)
      REFERENCES rooms(id),
FOREIGN KEY (leftRoomId)
      REFERENCES rooms(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `doors`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `doors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

