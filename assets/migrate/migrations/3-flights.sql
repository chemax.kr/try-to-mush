CREATE TABLE `flights` (
  `id` int(11) NOT NULL,
  `statusId` int(11) NOT NULL DEFAULT '0',
  `shipTypeId` int(11) NOT NULL DEFAULT '0',
  `missionId` int(11) NOT NULL DEFAULT '0',
  `cycleCount` int(11) NOT NULL DEFAULT '1',
  `armor` int(11) NOT NULL DEFAULT '50',
  `shield` int(11) NOT NULL DEFAULT '50',
  `shieldStatus` tinyint(1) NOT NULL DEFAULT '1',
  `fuel` int(11) NOT NULL DEFAULT '30',
  `oxygen` int(11) NOT NULL DEFAULT '30',
  `dateEnd` datetime DEFAULT NULL,
  `dateStart` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `crewCount` int(11) NOT NULL DEFAULT '0',
  `crewMax` int(11) NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `flights`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `flights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;