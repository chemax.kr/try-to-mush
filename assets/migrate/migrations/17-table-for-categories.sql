CREATE TABLE `characterDescriptionCategories` (
  `id` int(11) NOT NULL,
  `characterDescriptionId` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
    FOREIGN KEY (characterDescriptionId)
  REFERENCES characterDescriptions(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `characterDescriptionCategories`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `characterDescriptionCategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- statement

CREATE TABLE `characterCategories` (
  `id` int(11) NOT NULL,
  `characterId` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
    FOREIGN KEY (characterId)
  REFERENCES characters(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `characterCategories`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `characterCategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- statement
CREATE TABLE `itemCategories` (
`id` INT NOT NULL AUTO_INCREMENT ,
`itemId` INT NOT NULL ,
`name` VARCHAR(60) NOT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (itemId)
REFERENCES items(id)
  ) ENGINE = InnoDB;
CREATE TABLE `itemDescriptionCategories` (
`id` INT NOT NULL AUTO_INCREMENT ,
`itemDescriptionId` INT NOT NULL ,
`name` VARCHAR(60) NOT NULL ,
PRIMARY KEY (`id`),
  FOREIGN KEY (itemDescriptionId)
  REFERENCES itemDescriptions(id)
) ENGINE = InnoDB;
